<?php
 /*
 Template Name: Full width
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="col-xs-12">
				<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12">
			<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
			?>

			</div>
		</div><!-- .row -->
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
