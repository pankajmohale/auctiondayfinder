<?php
/**
 * auctiondayfinder functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package auctiondayfinder
 */

if ( ! function_exists( 'auctiondayfinder_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function auctiondayfinder_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on auctiondayfinder, use a find and replace
	 * to change 'auctiondayfinder' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'auctiondayfinder', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'auctiondayfinder' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'auctiondayfinder_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'auctiondayfinder_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function auctiondayfinder_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'auctiondayfinder_content_width', 640 );
}
add_action( 'after_setup_theme', 'auctiondayfinder_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function auctiondayfinder_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'auctiondayfinder' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'auctiondayfinder' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Menu Sidebar', 'auctiondayfinder' ),
		'id'            => 'sidebar-footer-menu',
		'description'   => esc_html__( 'Add widgets here.', 'auctiondayfinder' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'auctiondayfinder_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function auctiondayfinder_scripts() {
wp_enqueue_style( 'myfirstheme-style', get_stylesheet_uri() );
wp_enqueue_style( 'font-awesome', get_template_directory_uri() .'/css/font-awesome-4.7.0/css/font-awesome.min.css',array(),'4.7.0' );
 
if( !is_admin()){
    wp_deregister_script( 'jquery' );
    wp_register_script('jquery', get_template_directory_uri().'/js/jquery-3.1.1.min.js', false,'3.1.1',false);
    wp_enqueue_script('jquery');

    wp_register_script('jquerymigrate', get_template_directory_uri().'/js/jquery-migrate-1.4.1.min.js', false,'3.1.1',false);
    wp_enqueue_script('jquerymigrate');
}

	wp_enqueue_script( 'firstheme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'firstheme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_register_style( 'owlcarousel_css', get_template_directory_uri() .'/owl/owl.carousel.css', array(), '21');
	wp_enqueue_style( 'owlcarousel_css' );

	wp_register_style( 'classic', get_template_directory_uri() .'/css/classic.css', array(), '21');
	wp_enqueue_style( 'classic' ); 

	wp_register_style( 'classic_time', get_template_directory_uri() .'/css/classic.time.css', array(), '21');
	wp_enqueue_style( 'classic_time' ); 

	wp_register_style( 'classic_date', get_template_directory_uri() .'/css/classic.date.css', array(), '21');
	wp_enqueue_style( 'classic_date' );

/* ********************* Js ********************* */

	wp_register_script( 'picker', get_template_directory_uri() . '/js/picker.js', array(), '1.0.0', true);
	wp_enqueue_script( 'picker' );

	wp_register_script( 'picker_time', get_template_directory_uri() . '/js/picker.time.js', array(), '1.0.0', true);
	wp_enqueue_script( 'picker_time' );

	wp_register_script( 'picker_date', get_template_directory_uri() . '/js/picker.date.js', array(), '1.0.0', true);
	wp_enqueue_script( 'picker_date' );

	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom-js.js', array(), '1.0', true );
	wp_enqueue_script( 'time-picker-js', get_template_directory_uri() . '/js/time-picker-js.js', array(), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	    wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'auctiondayfinder_scripts' );

/**
 * navigation bootstrap
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load ajax function.
 */
require get_template_directory() . '/inc/ajaxfunction.php';

/**
 * Load gformsubmission function.
 */
require get_template_directory() . '/inc/gformsubmission.php';

/**
 * custom user function.
 */
require get_template_directory() . '/inc/userfunction.php';

/* 
 * Helper function to return the theme option value. If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * This code allows the theme to work without errors if the Options Framework plugin has been disabled.
 */

if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = false) {
	
	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
		
	if ( isset($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}

// Register Custom Post Type
function property_post_type() {

	$labels = array(
		'name'                  => _x( 'Properties', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Property', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Properties', 'text_domain' ),
		'name_admin_bar'        => __( 'Property', 'text_domain' ),
		'archives'              => __( 'Property Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Property:', 'text_domain' ),
		'all_items'             => __( 'All Properties', 'text_domain' ),
		'add_new_item'          => __( 'Add New Property', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Property', 'text_domain' ),
		'edit_item'             => __( 'Edit Property', 'text_domain' ),
		'update_item'           => __( 'Update Property', 'text_domain' ),
		'view_item'             => __( 'View Property', 'text_domain' ),
		'search_items'          => __( 'Search Property', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Property', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Property', 'text_domain' ),
		'items_list'            => __( 'Properties list', 'text_domain' ),
		'items_list_navigation' => __( 'Properties list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Properties list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Property', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
		
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'property', $args );

}
add_action( 'init', 'property_post_type', 0 );


/* Set ajax url */
add_action('wp_head','auctiondayfinder_ajaxurl');
function auctiondayfinder_ajaxurl() {
	?>
		<script type="text/javascript">
			var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		</script>
	<?php
}


/* =============================================================================================== 
										Create User Role 
================================================================================================== */
add_action( 'init', 'add_roles_insite' );
function add_roles_insite() {
	$result = add_role(
	    'agent_multilisting',
	    __( 'Agent Multilisting' ),
	    array(
	        'read'         => true,  // true allows this capability
	        'edit_posts'   => true,
	        'delete_posts' => true, // Use false to explicitly deny
	    )
	);

	$result = add_role(
	    'agent',
	    __( 'Agent' ),
	    array(
	        'read'         => true,  // true allows this capability
	        'edit_posts'   => true,
	        'delete_posts' => true, // Use false to explicitly deny
	    )
	);
     
	$result = add_role(
	    'owner',
	    __( 'Owner' ),
	    array(
	        'read'         => true,  // true allows this capability
	        'edit_posts'   => true,
	        'delete_posts' => true, // Use false to explicitly deny
	    )
	);

	// $result = add_role(
	//     'buyer',
	//     __( 'Buyer' ),
	//     array(
	//         'read'         => true,  // true allows this capability
	//     )
	// );
	// remove_role( 'buyer' );
}

/* =============================================================================================== 
										Create User Role 
================================================================================================== */
/**
 * Add a sidebar.
 */
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Login', 'auctiondayfinder' ),
        'id'            => 'login-sidebar',
        'description'   => __( 'Widgets in this area will be shown on all login pages.', 'auctiondayfinder' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );

/* Hide User admin bar */
if ( ! current_user_can( 'administrator' ) ) {
    show_admin_bar( false );
}