<?php
 /*
 Template Name: My Account
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="col-xs-12">
				<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12">
				<?php 
					if ( is_user_logged_in() ) {
					    $current_user = wp_get_current_user();
					    foreach ($current_user->roles as $rolekey => $rolevalue) {
						    if ($rolevalue == 'agent') {
								get_template_part( 'template-parts/user-template/content', 'agent' );
							} elseif ($rolevalue == 'agent_multilisting') {
								get_template_part( 'template-parts/user-template/content-agent', 'multilisting' );
						    } elseif ($rolevalue == 'owner') {
								get_template_part( 'template-parts/user-template/content', 'owner' );
						    } elseif ($rolevalue == 'subscriber') {
								get_template_part( 'template-parts/user-template/content', 'buyer' );
						    } elseif ($rolevalue == 'administrator') {
						    	get_template_part( 'template-parts/user-template/content', 'admin' );
						    	
						    }					    
					    }

					} else {
					    echo 'Welcome, visitor!';
					}				
				?>
			</div>
		</div><!-- .row -->
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
