jQuery(document).ready(function(){
    jQuery('.home_carousel').owlCarousel({
        stagePadding: 110,
        loop:true,
        margin:30,
        nav:true,
        dots:false,
        thumbs: false,
        thumbImage: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
    $.get("http://ipinfo.io", function (response) {
        $("#suburb_loc").val(response.city);
    }, "jsonp");

    jQuery('.user_carousel').owlCarousel({
        stagePadding: 0,
        loop:false,
        margin:30,
        nav:false,
        dots:false,
        thumbs: false,
        thumbImage: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
    $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
});
jQuery(document).ready(function(){
    var owl = $('.carousel_thumbnail');
    owl.owlCarousel({
        loop: true,
        items: 1,
        nav:false,
        dots:false,
        thumbs: true,
        thumbImage: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item',
    });
});

jQuery(document).ready(function($){

    jQuery('#suburb-result').on('click', '.list-group a', function() {
        console.log($(this).attr('data-value'));
        $('#suburb_loc').val($(this).attr('data-value'));
        $('#suburb-result').empty();
    });

});


/* ================= Save this proparty ================ */
/* Ajax for adding save property in user profile */
jQuery(document).ready(function($){
    /* Delete Property */
    jQuery('#suburb_loc').keypress(function(){
      var suburb_loc_key = $(this).val();
      // console.log(suburb_loc_key);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                    action:'suburb_loc_function',
                    suburb_loc : suburb_loc_key,
            },
            success:function(response){
                // console.log(suburb-result);
                $('#suburb-result').html(response);               
            }
        });
    });


    $(document).on('click','.property_save',function(){
      var proparty_id = $(this).attr('proparty-id');
      var current_user_id = $(this).attr('current_user_id');
        // console.log(proparty_id);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                    action:'save_property_function',
                    proparty_id : proparty_id,
                    current_user_id : current_user_id
            },
            success:function(response){
                if (response == 'in_array') {
                    alert('Property is already saved.');
                } else {
                    alert('Property is saved.');
                }
            }
        });
    });

    /* Delete Property */
    jQuery('.delete_property').click(function(){
      var proparty_id = $(this).attr('property_id');
      var current_user_id = $(this).attr('current_user_id');
        // console.log(proparty_id);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                    action:'delete_property_function',
                    proparty_id : proparty_id,
                    current_user_id : current_user_id,
            },
            success:function(response){
                alert('The property is deleted.');
                location.reload();
            }
        });
    });
    
    /* Delete Saved Property */
    jQuery('.delete_saved_property').click(function(){
      var proparty_id = $(this).attr('property_id');
      var current_user_id = $(this).attr('current_user_id');
        // console.log(proparty_id);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                    action:'delete_saved_property_function',
                    proparty_id : proparty_id,
                    current_user_id : current_user_id,
            },
            success:function(response){
                alert('The saved property is deleted.');
                location.reload();
            }
        });
    });

});