/* This is used for Timepicker */
jQuery(document).ready(function($){

    //jQuery('.custom-datepicker .gfield_time_hour #input_1_420_1').timepicker({ 'timeFormat': 'H' });
    /*jQuery('.custom-datepicker .ginput_container input').pickatime({
      // Escape any “rule” characters with an exclamation mark (!).
      clear: '',
      format: 'h:i A',
    });*/
    if ($('.start-time .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker = from_$input.pickatime('picker');

        var to_$input = $('.end-time .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker.get('value') ) {
          to_picker.set('min', from_picker.get('select'));
        }
        if ( to_picker.get('value') ) {
          from_picker.set('max', to_picker.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker.get('select');
            to_picker.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time .ginput_container input').on('change', function(event) {
           $from_time =  from_picker.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker.on('close', function(event) {
            if( from_picker.get('select') == null  && to_picker.get('select') == null ){
                from_picker.set('max', [23,30] );
                to_picker.set('min', [0,0] );
            }
        });
        to_picker.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker.get('select');
            from_picker.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker.on('close', function(event) {
              if( from_picker.get('select') == null  && to_picker.get('select') == null ){
                from_picker.set('max', [23,30] );
                to_picker.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-2 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-2 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_2 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-2 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_2 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_2.get('value') ) {
          to_picker_2.set('min', from_picker_2.get('select'));
        }
        if ( to_picker_2.get('value') ) {
          from_picker_2.set('max', to_picker_2.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_2.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_2.get('select');
            to_picker_2.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-2 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_2.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_2.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_2.on('close', function(event) {
            if( from_picker_2.get('select') == null  && to_picker_2.get('select') == null ){
                from_picker_2.set('max', [23,30] );
                to_picker_2.set('min', [0,0] );
            }
        });
        to_picker_2.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_2.get('select');
            from_picker_2.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_2.on('close', function(event) {
              if( from_picker_2.get('select') == null  && to_picker_2.get('select') == null ){
                from_picker_2.set('max', [23,30] );
                to_picker_2.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-3 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-3 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_3 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-3 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_3 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_3.get('value') ) {
          to_picker_3.set('min', from_picker_3.get('select'));
        }
        if ( to_picker_3.get('value') ) {
          from_picker_3.set('max', to_picker_3.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_3.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_3.get('select');
            to_picker_3.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-3 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_3.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_3.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_3.on('close', function(event) {
            if( from_picker_3.get('select') == null  && to_picker_3.get('select') == null ){
                from_picker_3.set('max', [23,30] );
                to_picker_3.set('min', [0,0] );
            }
        });
        to_picker_3.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_3.get('select');
            from_picker_3.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_3.on('close', function(event) {
              if( from_picker_3.get('select') == null  && to_picker_3.get('select') == null ){
                from_picker_3.set('max', [23,30] );
                to_picker_3.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-4 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-4 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_4 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-4 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_4 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_4.get('value') ) {
          to_picker_4.set('min', from_picker_4.get('select'));
        }
        if ( to_picker_4.get('value') ) {
          from_picker_4.set('max', to_picker_4.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_4.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_4.get('select');
            to_picker_4.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-4 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_4.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_4.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_4.on('close', function(event) {
            if( from_picker_4.get('select') == null  && to_picker_4.get('select') == null ){
                from_picker_4.set('max', [23,30] );
                to_picker_4.set('min', [0,0] );
            }
        });
        to_picker_4.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_4.get('select');
            from_picker_4.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_4.on('close', function(event) {
              if( from_picker_4.get('select') == null  && to_picker_4.get('select') == null ){
                from_picker_4.set('max', [23,30] );
                to_picker_4.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-5 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-5 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_5 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-5 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_5 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_5.get('value') ) {
          to_picker_5.set('min', from_picker_5.get('select'));
        }
        if ( to_picker_5.get('value') ) {
          from_picker_5.set('max', to_picker_5.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_5.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_5.get('select');
            to_picker_5.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-5 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_5.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_5.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_5.on('close', function(event) {
            if( from_picker_5.get('select') == null  && to_picker_5.get('select') == null ){
                from_picker_5.set('max', [23,30] );
                to_picker_5.set('min', [0,0] );
            }
        });
        to_picker_5.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_5.get('select');
            from_picker_5.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_5.on('close', function(event) {
              if( from_picker_5.get('select') == null  && to_picker_5.get('select') == null ){
                from_picker_5.set('max', [23,30] );
                to_picker_5.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-6 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-6 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_6 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-6 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_6 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_6.get('value') ) {
          to_picker_6.set('min', from_picker_6.get('select'));
        }
        if ( to_picker_6.get('value') ) {
          from_picker_6.set('max', to_picker_6.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_6.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_6.get('select');
            to_picker_6.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-6 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_6.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_6.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_6.on('close', function(event) {
            if( from_picker_6.get('select') == null  && to_picker_6.get('select') == null ){
                from_picker_6.set('max', [23,30] );
                to_picker_6.set('min', [0,0] );
            }
        });
        to_picker_6.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_6.get('select');
            from_picker_6.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_6.on('close', function(event) {
              if( from_picker_6.get('select') == null  && to_picker_6.get('select') == null ){
                from_picker_6.set('max', [23,30] );
                to_picker_6.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-7 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-7 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_7 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-7 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_7 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_7.get('value') ) {
          to_picker_7.set('min', from_picker_7.get('select'));
        }
        if ( to_picker_7.get('value') ) {
          from_picker_7.set('max', to_picker_7.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_7.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_7.get('select');
            to_picker_7.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-7 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_7.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_7.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_7.on('close', function(event) {
            if( from_picker_7.get('select') == null  && to_picker_7.get('select') == null ){
                from_picker_7.set('max', [23,30] );
                to_picker_7.set('min', [0,0] );
            }
        });
        to_picker_7.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_7.get('select');
            from_picker_7.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_7.on('close', function(event) {
              if( from_picker_7.get('select') == null  && to_picker_7.get('select') == null ){
                from_picker_7.set('max', [23,30] );
                to_picker_7.set('min', [0,0] );
            }
        });
    }


    if ($('.start-time-8 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-8 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_8 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-8 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_8 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_8.get('value') ) {
          to_picker_8.set('min', from_picker_8.get('select'));
        }
        if ( to_picker_8.get('value') ) {
          from_picker_8.set('max', to_picker_8.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_8.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_8.get('select');
            to_picker_8.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-8 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_8.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_8.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_8.on('close', function(event) {
            if( from_picker_8.get('select') == null  && to_picker_8.get('select') == null ){
                from_picker_8.set('max', [23,30] );
                to_picker_8.set('min', [0,0] );
            }
        });
        to_picker_8.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_8.get('select');
            from_picker_8.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_8.on('close', function(event) {
              if( from_picker_8.get('select') == null  && to_picker_8.get('select') == null ){
                from_picker_8.set('max', [23,30] );
                to_picker_8.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-9 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-9 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_9 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-9 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_9 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_9.get('value') ) {
          to_picker_9.set('min', from_picker_9.get('select'));
        }
        if ( to_picker_9.get('value') ) {
          from_picker_9.set('max', to_picker_9.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_9.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_9.get('select');
            to_picker_9.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-9 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_9.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_9.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_9.on('close', function(event) {
            if( from_picker_9.get('select') == null  && to_picker_9.get('select') == null ){
                from_picker_9.set('max', [23,30] );
                to_picker_9.set('min', [0,0] );
            }
        });
        to_picker_9.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_9.get('select');
            from_picker_9.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_9.on('close', function(event) {
              if( from_picker_9.get('select') == null  && to_picker_9.get('select') == null ){
                from_picker_9.set('max', [23,30] );
                to_picker_9.set('min', [0,0] );
            }
        });
    }

    if ($('.start-time-10 .ginput_container input').length != 0) {
        /* Pick up time */
        var from_$input = $('.start-time-10 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
        from_picker_10 = from_$input.pickatime('picker');

        var to_$input = $('.end-time-10 .ginput_container input').pickatime({
            clear: 'Clear',
        }),
            to_picker_10 = to_$input.pickatime('picker');

        // Check if there’s a “from” or “to” time to start with.
        if ( from_picker_10.get('value') ) {
          to_picker_10.set('min', from_picker_10.get('select'));
        }
        if ( to_picker_10.get('value') ) {
          from_picker_10.set('max', to_picker_10.get('select') );
        }

        // When something is selected, update the “from” and “to” limits.
        from_picker_10.on('set', function(event) {
          if ( event.select ) {
            $from_time =  from_picker_10.get('select');
            to_picker_10.set('min', [$from_time.hour,$from_time.mins+30] );
          }
        });

        jQuery('.start-time-10 .ginput_container input').on('change', function(event) {
           $from_time =  from_picker_10.get('select');
           if ($from_time != null) {
               if($from_time.hour == 0 && $from_time.mins == 0){
                to_picker_10.set('min', [$from_time.hour,$from_time.mins+30] );        
               }        
           }
        });

        from_picker_10.on('close', function(event) {
            if( from_picker_10.get('select') == null  && to_picker_10.get('select') == null ){
                from_picker_10.set('max', [23,30] );
                to_picker_10.set('min', [0,0] );
            }
        });
        to_picker_10.on('set', function(event) {
          if ( event.select ) {
            $to_time = to_picker_10.get('select');
            from_picker_10.set('max', [$to_time.hour,$to_time.mins-30] );
          }
        });
        to_picker_10.on('close', function(event) {
              if( from_picker_10.get('select') == null  && to_picker_10.get('select') == null ){
                from_picker_10.set('max', [23,30] );
                to_picker_10.set('min', [0,0] );
            }
        });
    }

});