<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package auctiondayfinder
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/css/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/css/owlcarousel/owl.theme.default.min.css"> -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body <?php body_class(); ?>>
	<?php 
		if(is_front_page()){
			$args = array(
				'posts_per_page'	=> 1,
				'post_type'		=> 'property',
				'meta_query'	=> array(
				array(
					'key'		=> 'is_featured',
					'value'		=> 'yes',
					'compare'	=> 'LIKE'
				)
			)
			); 

			$the_query = new WP_Query( $args );
		?>
		<?php if( $the_query->have_posts() ): ?>
			<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php 
					$exclude_id = get_the_ID();
					$background_img = wp_get_attachment_image_src( get_post_thumbnail_id(), "full" ); 
				?>
			<?php endwhile; ?>
		<?php endif; ?>

		<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
		<?php }else{

			$background_img = '';

		} ?>

	<div class="container-full" id="header_top" style="background-image:url('<?php echo $background_img[0]; ?>');">
		<div id="page" class="site container">
			<header id="masthead" class="site-header row" role="banner">
				<div class="site-branding col-md-4">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo of_get_option('logo'); ?>" alt="logo">
					</a>
				</div><!-- .site-branding -->
				<div class="col-md-8" id="top-menu">
					<nav class="navbar navbar-inverse " role="navigation">
						<?php if(!is_front_page()){ ?>
   					 	<form role="search" action="<?php echo site_url('/');?>" method="get" id="searchform" class="header-search-form">
						    <input type="text" name="s" value="<?php echo $_GET['s']; ?>"/>
						    <input type="hidden" name="post_type" value="property" />  
					  	</form>
					  	<?php } ?>
				        <!-- Brand and toggle get grouped for better mobile display -->
				        <div class="navbar-header">
				            <button type="button"  class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div><!--end navbar-header-->
        				<div class="collapse navbar-collapse menu-primary" id="bs-example-navbar-collapse-1">
				            <?php
				            wp_nav_menu( array(
				                'menu'              => 'Menu1',
				                'theme_location'    => 'primary',
				                'depth'             => 2,
				                'container'         => '',
				                'container_class'   => 'collapse navbar-collapse',
				                'container_id'      => 'bs-example-navbar-collapse-1',
				                'menu_class'        => 'nav navbar-nav',
				                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				                'walker'            => new wp_bootstrap_navwalker())
				            );
				            ?>
   					 	</div><!--end navbar-colapse-->
	 				</nav>
				</div>
			</header><!-- #masthead -->
			<?php if(is_front_page()){ ?>
				<div class="row" id="home_page_only">
					<div class="col-md-12">
						<h1><?php echo of_get_option('banner_text'); ?></h1>
						<p><?php echo of_get_option('banner_subtext'); ?></p>
						<form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
						    <input type="text" name="s" placeholder="Enter suburb, region" id="suburb_loc" />
						    <div id="suburb-result"></div>
						    <input type="hidden" name="post_type" value="property" /> 
						    <div class="inclined_submit">
							    <!-- <p class="inclined"></p> -->
							    <input type="submit" alt="Search" value="Search listing" />
							    <span></span>
							</div>    
					  	</form>
					</div>
				</div>
			<?php } ?>
			
		</div>	
		<?php if (is_front_page()) { ?>
			<a id="featured-property-link" href="<?php echo get_permalink($exclude_id); ?>"></a>
		<?php } ?>	
	</div>	
	<?php if (is_front_page()) { ?>
		<div class="container-full" id="header_carousel">
			<div class="carousel-title text-center"><h1><?php echo of_get_option('slider_title'); ?></h1></div>
			<div class="owl-carousel home_carousel owl-theme">
				<?php 
				$date = date('Ymd');
				$args1 = array(
					'posts_per_page'=> -1,
					'post_type'		=> 'property',
					'post_status'	=> 'publish',
					'post__not_in'	=> array($exclude_id),
					'meta_key'		=> 'auction_date',
					'orderby'		=> 'meta_value',
					'order'			=> 'ASC',
					'meta_query'	=> array(
						array(
							'key'		=> 'auction_date',
							'compare'	=> '>=',
							'value'		=> $date,
						)
					)
				); 

			$the_query1 = new WP_Query( $args1 );
		?>
		<?php if( $the_query1->have_posts() ): ?>
			<?php while( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
				<div class="item">
					<div class="property_image">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="peoperty_desc">
						<p class="property_address">
							<span class="property_plot"> <?php the_field('property_name'); ?></span>
							<span class="property_plot"> <?php the_field('plot_no'); ?></span>
							<span class="property_street"> <?php the_field('street_name'); ?></span>
							<span class="property_suburb"> <?php the_field('suburb'); ?>, </span>
							<span class="property_state"> <?php the_field('state'); ?></span>
							<span class="property_postcode"> <?php the_field('postcode'); ?></span>
						</p>
						<p>
							<span class="property_size"> <?php the_field('land_size_in_sum'); ?></span>
							<span class="property_unit"> <?php the_field('land_size_unit'); ?></span>
							<span class="property_bed"> <?php the_field('bedrooms'); ?></span>
							<span class="property_bath"> <?php the_field('bathrooms'); ?></span>
							<span class="property_car"> <?php the_field('carapaces_or_garage'); ?></span>
						</p>
						<a class="property_read_more" href="<?php the_permalink(); ?>">View details</a>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query();	 ?>
			</div>
		</div>
		<?php $home_img = wp_get_attachment_image_src( get_post_thumbnail_id(), "full" ); ?>
		<div id="content" class="site-content container-full" style="background-image:url('<?php //echo $home_img[0]; ?>');">
	<?php } elseif ( is_page_template( 'template-login.php' ) ) { ?>
		<div id="content" class="custom-template">
	<?php } else { ?>
		<div id="content" class="main-content container">
	<?php } ?>