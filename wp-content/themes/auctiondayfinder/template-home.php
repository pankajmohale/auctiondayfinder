<?php
 /*
 Template Name: Home Full Width
 */
 ?>

<?php 
get_header(); ?>
<div class="container" id="home_feature">
	<div class="row">
		<div class="col-md-6 col-xs-12" id="feature1">
			<div class="feature_img"><img src="<?php the_field('feature_image_1'); ?>" /></div>
			<h2><?php the_field('feature_title_1'); ?></h2>
			<?php the_field('feature_text_1'); ?>
			<p><a class="read_more" href="<?php the_field('feature_link_1'); ?>"><?php the_field('feature_link_text_1'); ?></a></p>
		</div>
		<div class="col-md-6 col-xs-12" id="feature2">
			<div class="feature_img"><img src="<?php the_field('feature_image_2'); ?>" /></div>
			<h2><?php the_field('feature_title_2'); ?></h2>
			<?php the_field('feature_text_2'); ?>
			<p><a class="read_more" href="<?php the_field('feature_link_2'); ?>"><?php the_field('feature_link_text_2'); ?></a></p>
		</div>
	</div>
<?php
get_footer();