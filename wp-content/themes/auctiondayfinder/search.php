<?php session_start(); ?>
<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package auctiondayfinder
 */
get_header(); ?>
<div class="row">
<div class="col-md-12 col-xs-12" id="property_listing">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		$date = date('Ymd');
		if ($_GET['s'] == '') {
			$args = array(
				'posts_per_page'=> -1,
				'post_type'		=> 'property',
				'post_status'	=> 'publish',
			);
		} else {
			$args = array(
				'posts_per_page'=> -1,
				'post_type'		=> 'property',
				'post_status'	=> 'publish',
				
				'meta_query'	=> array(
	                'relation' => 'OR',
					array(
	                    'key' => 'state',
	                    'compare' => '=',
	                    'value' => $_GET['s']
	                ),
	                array(
	                    'key' => 'postcode',
	                    'compare' => '=',
	                    'value' => $_GET['s']
	                ),
	                array(
	                    'key' => 'suburb',
	                    'compare' => '=',
	                    'value' => $_GET['s']
	                ),
				)
			);			
		}

		$the_query = new WP_Query( $args );
		$posts_date_array = array();
		if ( $the_query->have_posts() ) { ?>
			<?php if($the_query->post_count <= 1){
				$auction = 'auction';
			}else{
				$auction = 'auctions';
			} ?>
			<header class="page-header">
				<h1 class="page-title">Found <?php echo $the_query->post_count; ?> <?php echo $auction; ?> for <?php echo get_search_query(); ?></h1>
			</header><!-- .page-header -->
			<?php 
			while ( $the_query->have_posts() ) {
				$the_query->the_post(); 
				$date_ts = strtotime(get_field('auction_date'));
				$posts_date_array[$date_ts][] = $the_query->post;
			}
			// if sort decending order.
			// krsort($posts_date_array);

			// if sort ascending order.
			ksort($posts_date_array);
			?>
			<div class="row">
			<?php
			if(!empty($posts_date_array)) {
				foreach($posts_date_array as $date_ts => $posts) {
					if(!empty($posts)) {
						?>
						<div class="col-md-12">
							<p class="property_single_date">
								<?php 
									echo 'on ' . date('l j, F', $date_ts);
								?>
							</p>
						</div>
						<?php
						foreach($posts as $post) {
							?>
							<div class="col-md-3">
								<?php 
								$post_id = $post->ID;
								$result_post_id[] = $post_id; 
								?>
								<div class="item-wrapper">
									<div class="item">
										<div class="property_image">
											<?php echo get_the_post_thumbnail($post_id); ?>
										</div>
										<div class="peoperty_desc">
											<p class="property_address">
												<span class="property_plot"> <?php echo get_field('plot_no', $post_id); ?></span>
												<span class="property_street"> <?php echo get_field('street_name', $post_id); ?></span>
												<span class="property_suburb"> <?php echo get_field('suburb', $post_id); ?>, </span>
												<span class="property_state"> <?php echo get_field('state', $post_id); ?></span>
												<span class="property_postcode"> <?php echo get_field('postcode', $post_id); ?></span>
											</p>
											<p>
												<span class="property_size"> <?php echo get_field('land_size_in_sum', $post_id); ?></span>
												<span class="property_unit"> <?php echo get_field('land_size_unit', $post_id); ?></span>
												<span class="property_bed"> <?php echo get_field('bedrooms', $post_id); ?></span>
												<span class="property_bath"> <?php echo get_field('bathrooms', $post_id); ?></span>
												<span class="property_car"> <?php echo get_field('carapaces_or_garage', $post_id); ?></span>
											</p>
											<a class="property_read_more" href="<?php echo get_permalink($post_id); ?>">View details</a>
										</div>
									</div>
								</div>
							</div>
							<?php
						}						
					}
				}
			}		
			?>
			</div><!--- ./row -->
			<?php	
		} else {
			get_template_part( 'template-parts/content', 'none' );
		}
		?>
		</main><!-- #main -->
	</section><!-- #primary -->
	<?php 
		/* Send data to individual page for next and previous feature */
		/*echo $_SERVER['REQUEST_URI'];
		echo "<br />";
		echo $_SERVER['QUERY_STRING'];*/
		
		/*$findpage_url = site_url().'/?'.$_SERVER['QUERY_STRING'];*/

		$_SESSION["postidarray"] = $result_post_id; 
		// $_SESSION["find_text"] = get_search_query(); 
		// $_SESSION["findpage_url"] = $findpage_url; 
		// $_SESSION["search_suburb"] = $_GET['s']; 
	?>

</div>
<?php
get_footer();
