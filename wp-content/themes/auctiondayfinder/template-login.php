<?php
 /*
 Template Name: Login
 */
get_header(); ?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="row">
				<div class="col-xs-12">
					<div class="signin-top">
						Don't have an account yet? <a class="sign-up-here" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Sign up' ) ) ); ?>">Sign up here</a>
					</div><!-- .entry-header -->
				</div>

				<div class="col-xs-12">
					<div class="entry-header sign-in">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div><!-- .entry-header -->
				</div>
				<div class="col-sm-6 col-md-6 col-xs-12">
				<?php if ( is_active_sidebar( 'login-sidebar' ) ) : ?>
					<div id="login-page" class="login-wrapper signin"> 
						<?php dynamic_sidebar( 'login-sidebar' ); ?>
					</div>
				<?php endif; ?>

				</div>
				<div class="col-sm-6 col-md-6 col-xs-12" >
					<?php get_sidebar(); ?>	
				</div><!-- .col-md-4>-->
			</div><!-- .row -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();
