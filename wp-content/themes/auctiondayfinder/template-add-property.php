<?php
 /*
 Template Name: Add Property
 */
 ?>

<?php 
get_header();
	if ( is_user_logged_in() ) {
	
	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	foreach ($current_user->roles as $userkey => $uservalue) {
		if ($uservalue == 'agent_multilisting') {  ?>
		<div class="container" id="property_feature">
			<div class="row">
				<div class="col-xs-12">
					<div class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div><!-- .entry-header -->
				</div>
				<div class="col-xs-12 col-sm-6">
					<?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]'); ?>
				</div>
				<div class="col-xs-12 col-sm-6">

				</div>
			</div>
		<?php	} elseif ($uservalue == 'agent') { ?>
			<div class="container" id="property_feature">
				<div class="row">
					<div class="col-xs-12">
						<div class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
					</div>
					<div class="col-xs-12 col-sm-6">
						<?php 
							// $current_user = wp_get_current_user();
							// $user_id = $current_user->ID;					    
							// $user_add_property_val = get_user_meta( $user_id,  'can_add_property', true );
						?>
						<?php echo do_shortcode('[gravityform id="10" title="false" description="false" ajax="true"]'); ?>
					
					</div>
					<div class="col-xs-12 col-sm-6">

					</div>
				</div>
		<?php } elseif ($uservalue == 'owner') { ?>
			<div class="container" id="property_feature">
				<div class="row">
					<div class="col-xs-12">
						<div class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
					</div>
					<div class="col-xs-12 col-sm-6">
						<?php echo do_shortcode('[gravityform id="10" title="false" description="false" ajax="true"]'); ?>
					</div>
					<div class="col-xs-12 col-sm-6">

					</div>
				</div>
		<?php } elseif ($uservalue == 'administrator') { ?>
			<div class="container" id="property_feature">
				<div class="row">
					<div class="col-xs-12">
						<div class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
					</div>
					<div class="col-xs-12 col-sm-6">
						<?php echo do_shortcode('[gravityform id="10" title="false" description="false" ajax="true"]'); ?>
					</div>
					<div class="col-xs-12 col-sm-6">

					</div>
				</div>
		<?php } else { ?>
			<div class="container" id="property_feature">
				<div class="row">
					<div class="col-xs-12">
						<div class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
					</div>
					<div class="col-xs-12 col-sm-6">
						<?php 
							echo "You do not have permission to access the property.";
						?>
					</div>
					<div class="col-xs-12 col-sm-6">

					</div>
				</div>
		<?php } ?>

	<?php } ?>
	<?php } else { ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
			</div>
			<div class="col-xs-12 sign-up">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			</div>
			<div class="col-sm-6 col-md-6 col-xs-12">
				<?php if ( is_active_sidebar( 'login-sidebar' ) ) : ?>
					<div id="login-page" class="login-wrapper signin"> 
						<?php dynamic_sidebar( 'login-sidebar' ); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-xs-12">
				<div class="signin-top">
					Don't have an account yet? <a class="sign-up-here" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Sign up' ) ) ); ?>">Sign up here</a>
				</div><!-- .entry-header -->
			</div>
		</div><!-- .row -->	
<?php }
get_footer();