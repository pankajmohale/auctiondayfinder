<?php
 /*
 Template Name: Update Posts
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="col-xs-12">
				<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12">
				<?php
					$update_post_id = $_GET['post_id'];
					echo do_shortcode('[gravityform id="6" update="'.$update_post_id.'"]');
				?>
			</div>
		</div><!-- .row -->
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
