<?php session_start(); ?><?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package auctiondayfinder
 */

get_header(); ?>
<?php
	$get_postid_from_session = $_SESSION["postidarray"];
?>
<div class="row">
<div class="col-md-12 col-xs-12">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php 
			if ($get_postid_from_session) {
				$current = array_search(get_the_ID(), $get_postid_from_session);
				$prevID = $get_postid_from_session[$current-1];
				$nextID = $get_postid_from_session[$current+1];
			}
		?>

		<div class="single-navigation">
			<div class="row">
				<div class="col-sm-8 col-md-8">
					<?php $get_suburb = get_field( "suburb" ); ?>
					<div class="back-search-link breadcrumbs-links"><a href="<?php echo site_url().'/?s='. $get_suburb .'&post_type=property'; ?>"><?php echo $get_suburb; ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i> </a></div>
					<?php the_title( '<div class="title-wrapper breadcrumbs-links"><h2 class="entry-title">', '</h2></div>' ); ?>
				</div>
				<!-- <div class="alignleft">
					<a href="<?php echo get_permalink($prevID); ?>"
					  title="<?php echo get_the_title($prevID); ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev property</a>
				</div> -->
				<?php if (!empty($nextID)) { ?>
					<div class="col-sm-4 col-md-4">
						<div class="next-post-link breadcrumbs-links alignright"><a href="<?php echo get_permalink($nextID); ?>" 
						 title="<?php echo get_the_title($nextID); ?>">Next property <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
					</div>
				<?php } else { ?>
					<div class="col-sm-4 col-md-4">
						<div class="next-post-link breadcrumbs-links alignright">
							<?php 
								$next_post = get_next_post();
								if($next_post) {
									$next_title = strip_tags(str_replace('"', '', $next_post->post_title));
									echo "\t" . '<a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class=" ">Next property <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>' . "\n";
								}
							?>						
						</div>
					</div>
				<?php } ?>
			</div>
		</div><!-- .navigation -->

		<?php
		while ( have_posts() ) : the_post();

			/*the_post_navigation( array(
				'prev_text'                  => __( '<i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev property' ),
				'next_text'                  => __( 'Next property <i class="fa fa-angle-double-right" aria-hidden="true"></i>' ),
				'screen_reader_text' => __( 'Post navigation' ),
			));*/

			get_template_part( 'template-parts/content', 'property' );

			// If comments are open or we have at least one comment, load up the comment template.
			/*if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;*/

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();
