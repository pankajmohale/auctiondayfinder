<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package auctiondayfinder
 */

?>
</div><!-- #content -->
</div><!-- .container -->
<?php 
	if (is_singular( 'property' )) { 


?>
	<div class="container-fluid property-share-info">
		<div class="container">
			<div class="property-meta">
				<div class="row">
					<div class="col-sm-3 col-md-3 border-rg print-one-forth">
						<?php if ( is_user_logged_in() ) { ?>
						    <div class="save-property property-share"><a class="property_save" proparty-id="<?php the_ID(); ?>" current_user_id="<?php echo get_current_user_id(); ?>"><i class="fa fa-tag" aria-hidden="true"></i> Save this property</a></div>
						<?php } else { ?>
						    <div class="save-property property-share"><a href="" class="" data-toggle="modal" data-target="#loginpopup"><i class="fa fa-tag" aria-hidden="true"></i> Save this property</a></div>
						    
						<?php } ?>
					</div>
					<div class="col-sm-3 col-md-3 border-rg print-one-forth">
						<?php if ( is_user_logged_in() ) { ?>
							<div class="print-page property-share"><a href="javascript:window.print()"><i class="fa fa-file" aria-hidden="true"></i> Print This Page</a> </div>
						<?php } else { ?>
						    <div class="print-page property-share"><a href="" class="" data-toggle="modal" data-target="#loginpopup"><i class="fa fa-file" aria-hidden="true"></i> Print This Page</a> </div>
						<?php } ?>				
					</div>
					<div class="col-sm-3 col-md-3 border-rg print-one-forth">
					<!-- Button trigger modal -->
					<?php if ( is_user_logged_in() ) { ?>
						<div class="send-to-friend property-share">
							<a href="" class="" data-toggle="modal" data-target="#senttofriend"><i class="fa fa-rss" aria-hidden="true"></i> Send to a friend</a>							
						</div>
					<?php } else { ?>
					    <div class="send-to-friend property-share">
							<a href="" class="" data-toggle="modal" data-target="#loginpopup"><i class="fa fa-rss" aria-hidden="true"></i> Send to a friend</a>							
						</div>
					<?php } ?>	
					<!-- End Model box -->
					</div>
					<?php 
						$suburb = $_SESSION["search_suburb"];
						$propartyname = get_the_title();
					?>
					<div class="col-sm-3 col-md-3 border-rg last print-one-forth">
					<?php if ( is_user_logged_in() ) { ?>
						<div class="get-directions property-share"><a href="http://maps.google.com/maps?saddr=&daddr=<?php echo $propartyname; ?>" target="_blank"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get directions</a></div>
					<?php } else { ?>
						<div class="get-directions property-share"><a href="" class="" data-toggle="modal" data-target="#loginpopup"><i class="fa fa-location-arrow" aria-hidden="true"></i> Get directions</a></div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal box for sent to a friend popup -->
			<div class="modal fade" id="senttofriend" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <span class="modal-title" id="exampleModalLongTitle">Send to a friend</span>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <?php 
			        	$person = get_field('agent');
			        	$agent_mail = $person['user_email'];
			        	$agent_fname = $person['user_firstname'];
			        	$agent_lname = $person['user_lastname'];
			        	$agent_name = $agent_fname .' '. $agent_lname;
			        	$site_url = get_permalink();
			        	$proparty_name = get_the_title();
			        	echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true" field_values="site_url='.$site_url.'&amp;proparty_name='.$proparty_name.'&amp;agent_mail='.$agent_mail.'&amp;agent_name='.$agent_name.'"]'); ?>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        </div>
			    </div>
			  </div>
			</div>
		<!-- Send to friend modal end -->

		<!-- Modal box for Login popup -->
			<div class="modal fade" id="loginpopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <span class="modal-title" id="exampleModalLongTitle">Please Login First</span>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body text-center">
			        <a href="#" class="btn btn-skyblue skyblue">Login</a>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        </div>
			    </div>
			  </div>
			</div>
		<!-- Login button modal end -->


	</div>	
<?php 
	}
?>
<div class="container-full" id="footer-top">
	<footer id="colophon" class="site-footer container" role="contentinfo">
		<div class="col-md-3 print-col-3" id="copyright">
			<div class="footer-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo of_get_option('footer_logo'); ?>" alt="logo">
				</a>
			</div>
			<div class="footer-copyright">
				<p>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">AuctionDayFinder<span>.com.au</span></a>
				</p>
				<p class="copyright-text">
					<?php echo of_get_option('footer_copyright'); ?>
				</p>
			</div>
		</div>
		<div class="col-md-6 print-col-6" id="footer-menu">
			<?php dynamic_sidebar('sidebar-footer-menu'); ?>
		</div>
		<div class="col-md-3 print-col-3" id="footer-social">
			<ul class="clearfix">
				<?php if(of_get_option('facebook_url')){ ?>
					<li class="facebook-social"><a href="http://<?php echo of_get_option('facebook_url'); ?>" target="_blank" alt="facebook"></a></li>
				<?php } ?>
				<?php if(of_get_option('twitter_url')){ ?>
				<li class="twitter-social"><a href="http://<?php echo of_get_option('twitter_url'); ?>" target="_blank" alt="twitter"></a></li>
				<?php } ?>
				<?php if(of_get_option('instagram_url')){ ?>
				<li class="instagram-social"><a href="http://<?php echo of_get_option('instagram_url'); ?>" target="_blank" alt="instagram"></a></li>
				<?php } ?>
				<?php if(of_get_option('pinterest_url')){ ?>
				<li class="pinterest-social"><a href="http://<?php echo of_get_option('pinterest_url'); ?>" target="_blank" alt="pinterest"></a></li>
				<?php } ?>
			</ul>
		</div>
	</footer>
</div>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="<?php echo get_stylesheet_directory_uri();?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri();?>/owl/OwlCarousel2Thumbs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.js"></script>
<?php wp_footer(); ?>
</body>
</html>
