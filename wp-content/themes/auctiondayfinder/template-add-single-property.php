<?php
 /*
 Template Name: Add Single Property
 */
 ?>

<?php 
get_header(); ?>
<div class="container" id="property_feature">
	<div class="row">
	<div class="col-xs-12">
		<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
		</div>
		<div class="col-xs-12 col-sm-6">
			<?php echo do_shortcode('[gravityform id="10" title="false" description="false" ajax="true"]'); ?>
		</div>
		<div class="col-xs-12 col-sm-6">

		</div>
	</div>
<?php
get_footer();