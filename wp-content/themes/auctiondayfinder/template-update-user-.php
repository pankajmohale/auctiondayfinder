<?php
 /*
 Template Name: update_user
 */
 ?>

<?php 
get_header(); ?>
<div class="container" id="property_feature">
	<div class="row">
		<div class="col-xs-12">
			
		</div>


		
		<div class="col-xs-12 col-sm-6">
			<?php echo do_shortcode('[gravityform id="8" title="false" description="false" ajax="true"]'); ?>
		</div>
		<div class="col-xs-12 col-sm-6">

		</div>
	</div>

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	</div>
<?php
get_footer();