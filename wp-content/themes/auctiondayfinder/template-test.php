<?php
 /*
 Template Name: Test
 */
 ?>

<?php 
get_header(); ?>
<div class="container" id="home_feature">
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="slider">
				<div class="owl-carousel carousel_thumbnail" data-slider-id="2">
				    <div class="item">
				        <img src="http://localhost/auctiondayfinder/wp-content/uploads/2017/01/slider2.png" alt="photo by Joshua Earle">
				    </div>
				    <div class="item">
				        <img src="http://localhost/auctiondayfinder/wp-content/uploads/2017/01/slider3.png" alt="photo by Alexander Dimitrov">
				    </div>
				    <div class="item">
				        <img src="http://localhost/auctiondayfinder/wp-content/uploads/2017/01/slider2.png" alt="photo by Wojciech Szaturski">
				    </div>
				    <div class="item">
				        <img src="http://localhost/auctiondayfinder/wp-content/uploads/2017/01/slider3.png" alt="photo by Kyle Szegedi">
				    </div>
				</div>
			</div><!-- Slider -->
		</div>
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	</div>
<?php
get_footer();