<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package auctiondayfinder
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('print_wrapper'); ?>>
	<!-- <header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php auctiondayfinder_posted_on(); ?>
		</div>
		<?php
		endif; ?>
	</header> --><!-- .entry-header -->
	<div class="row">
		<div class="col-sm-6 col-md-6 padding-right print-half-left">
			<div class="thumbnail_wrapper">
				<?php 
					$images = get_field('gallery');

				//if( $images ): ?>
				<?php if( $images && has_post_thumbnail() ){ ?>
					<div class="slider">
						<div class="owl-carousel carousel_thumbnail" data-slider-id="2">
								<div class="item">
							        <img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" alt="" />
							    </div>
						    <?php foreach( $images as $image ): ?>
							    <div class="item">
							        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							    </div>
				            <?php endforeach; ?>
						</div>
					</div><!-- Slider -->
				<?php } elseif (has_post_thumbnail()) { ?>
					<div class="slider">
						<div class="owl-carousel carousel_thumbnail" data-slider-id="2">
							<div class="item">
						        <img src="<?php echo the_post_thumbnail_url( 'full' ); ?>" alt="" />
						    </div>
						</div>
					</div><!-- Slider -->
				<?php } else { ?>
					<div class="slider">
						<div class="owl-carousel carousel_thumbnail" data-slider-id="2">
							<div class="item">
						        <img src="<?php echo get_template_directory_uri() . '/img/placeholder.png' ?>">
						    </div>
						</div>
					</div><!-- Slider -->
				<?php } ?>

			</div>			
			<div class="property-info">
				<div class="row">
					<div class="col-sm-3 col-md-3 print-one-forth">
						<div class="area-info pro-info">
							<img src="<?php echo get_template_directory_uri() . '/img/sqft.png' ?>"> <?php echo get_field( "building_area" ); ?>
							<?php echo get_field( "land_size_unit" ); ?>					
						</div>						
					</div>
					<div class="col-sm-3 col-md-3 print-one-forth">
						<div class="bedrooms-info pro-info">
							<img src="<?php echo get_template_directory_uri() . '/img/beadroom.png' ?>"> <?php echo get_field( "bedrooms" ); ?> bedrooms
						</div>						
					</div>
					<div class="col-sm-3 col-md-3 print-one-forth">
						<div class="bathrooms-info pro-info">
							<img src="<?php echo get_template_directory_uri() . '/img/bathroom.png' ?>"> <?php echo get_field( "bathrooms" ); ?> bathrooms<br/>
						</div>						
					</div>
					<div class="col-sm-3 col-md-3 print-one-forth">
						<div class="parking-info pro-info">
							<img src="<?php echo get_template_directory_uri() . '/img/parking.png' ?>"> <?php echo get_field( "parking" ); ?>x Parking<br/>
						</div>						
					</div>
				</div>
			</div>
			&nbsp;
			<div class="property-info-tabs">
				<div class="row">
					<script>
					  $( function() {
					    $( "#tabs" ).tabs();
					  } );
					</script>
					<div id="tabs">
					  <ul>
					    <li><a href="#tabs-1">Basic Information</a></li>
					    <li><a href="#tabs-2">Property Features</a></li>
					    <li><a href="#tabs-3">Auction Details & Location</a></li>
					  </ul>
					  <div id="tabs-1">
					  	<b>Property Name</b>: <?php echo get_field( "property_name" ); ?> <br/>
					  	<br/>
					  	<b>Unit Number</b>: <?php echo get_field( "unit_no" ); ?> | <b>Street Number</b>: <?php echo get_field( "street_no" ); ?> | <b>Street Name</b>: <?php echo get_field( "street_name" ); ?> <br/>
					  	<br/>
					  	<b>Suburb</b>: <?php echo get_field( "suburb" ); ?> | <b>Postcode</b>: <?php echo get_field( "postcode" ); ?> <br/>
					  </div>
					  <div id="tabs-2">
					    <b>Property Type</b>: <?php echo get_field( "property_type" ); ?> | <b>Bedrooms</b>: <?php echo get_field( "bedrooms" ); ?> | <b>Bathrooms</b>: <?php echo get_field( "bathrooms" ); ?> <br/>
					    <br />
					    <b>Parking</b>: <?php echo get_field( "parking" ); ?>x | <b>Building Area</b>: <?php echo get_field( "building_area" ); ?> | <b>Land Area</b>: <?php echo get_field( "land_area" ); ?> <br/>
					    <br/>
					    <b>Energy Efficiency</b>: <?php echo get_field( "energy_efficiency" ); ?> <br/>
					    <br/>
					    <?php $content = get_the_content(); ?>
					    <b>Property Description</b>: <?php print $content; ?> <br/>
					    <br/>
					    <b>Price Range Minimum</b>: <?php echo get_field( "price_range_minimum" ); ?> | <b>Price Range Maximum</b>: <?php echo get_field( "price_range_maximum" ); ?> <br/>
					  </div>
					  <div id="tabs-3">
					    <b>Auction Date</b>: <?php echo date("d-m-Y", strtotime(get_field( "auction_date" ))); ?> | <b>Auction Time</b>: <?php echo get_field( "auction_time" ); ?> <br/>
					    <br/>
					    <b>Auction Venue</b>: <?php echo get_field( "auction_venue_name" ); ?>
					  </div>
					</div>
				</div>
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-1"></div>
		<div class="col-sm-6 col-md-5 print-half-right">
			<div id="inspection-wrapper">
				<div class="inspection_wrapper">
					<div class="inspection-title">
						<h2>Inspection Opportunities</h2>
					</div>
					<div class="inspection-inner">
					<?php 
						for ($insloop = 1; $insloop <= 10 ; $insloop++) { ?>
						<?php if (get_field('inspection_date_'.$insloop)) { ?>
							<div class="inspection-info">
								<div class="inspection-date">
									<?php $dateval = get_field('inspection_date_'.$insloop); ?>
									<?php echo date("D jS F", strtotime($dateval) ); ?>
								</div>
								<div class="inspection-timing">
									<span class="time">
									<?php echo get_field('inspection_start_time_'.$insloop); ?> - <?php echo get_field('inspection_end_time_'.$insloop); ?>
									</span>
									<!-- <span class="calender-icon">
										<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
									</span> -->
								</div>
							</div>
						<?php } ?>							
					<?php 
						}
					?>
					</div>
				</div>
				<div class="agent-data">
					<div class="agent-title">
						<h2>Auction held by</h2>
					</div>
					<?php
					$fname = get_the_author_meta( 'first_name' );
					$lname = get_the_author_meta( 'last_name' );
					$phoneno = get_the_author_meta( 'phone_number' );
					$userprofilepicture = get_the_author_meta( 'user_profile_picture' );
					$agent_mail = get_the_author_meta( 'user_email' );
					$proparty_name = get_the_title();
					?>
						<div class="col-xs-3 padding-left print-avatar">
							<div class="agent-avatar">
								<?php if ($userprofilepicture) { ?>
									<img src ="<?php echo $userprofilepicture; ?>" />
								<?php } else { ?>
									<img src ="<?php echo get_template_directory_uri().'/img/agent.png';?>" />
								<?php } ?>
							</div>						
						</div>
						<div class="col-xs-9 padding-left print-agent-name">
							<div class="agent-name-wrapper">
								<div class="agent-name">
								<?php if ($fname && $lname) { ?>
									<?php echo $fname.' '.$lname; ?>
								<?php } else {
										echo 'Unknown';
									} ?>
								</div>
								<div class="agent-phone">
									<?php if ($phoneno) { ?>
										<i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo $phoneno; ?>"><?php echo $phoneno; ?></a>
									<?php } else { ?>
										<i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo $phoneno; ?>"><?php echo 'XXXX-XXX-XXX'; ?></a>
									<?php } ?>
								</div>
							</div>
						</div>
				</div>
				<div class="agent-contact-buttons">
					<div class="interestd-btn">
					<!-- Button trigger modal -->
						<a href="" class="btn btn-sliver" data-toggle="modal" data-target="#iaminterestedModal">I'm interested...</a>
					<!-- End Model box -->
					</div>
					<div class="email-agent-btn">
						<a href="mailto:<?php echo $agent_mail; ?>?subject=<?php echo $proparty_name; ?>" class="btn btn-skyblue skyblue">E-mail agent <img src="<?php echo get_template_directory_uri() . '/img/btn-arrow.png' ?>"></a>
					</div>
				</div>
				<!-- Modal box for i am interested form popup -->
					<div class="modal fade" id="iaminterestedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <span class="modal-title" id="exampleModalLongTitle">I'm interested form</span>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true" field_values="agent_email='.$agent_mail.'&amp;proparty_name='.$proparty_name.'"]'); ?>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        </div>
					    </div>
					  </div>
					</div>
				<!-- modal end -->
			</div>
		</div>
	</div>
	
	<!-- <div class="entry-content">
		<?php
			// the_content( sprintf(
			// 	/* translators: %s: Name of current post. */
			// 	wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'auctiondayfinder' ), array( 'span' => array( 'class' => array() ) ) ),
			// 	the_title( '<span class="screen-reader-text">"', '"</span>', false )
			// ) );

			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'auctiondayfinder' ),
			// 	'after'  => '</div>',
			// ) );
		?>
	</div> --><!-- .entry-content -->

	<!-- <footer class="entry-footer">
		<?php auctiondayfinder_entry_footer(); ?>
	</footer> --><!-- .entry-footer -->
</article><!-- #post-## -->