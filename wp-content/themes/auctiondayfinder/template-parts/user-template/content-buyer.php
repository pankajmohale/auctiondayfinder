<?php
/**
 * Template part for displaying user page content in template-my-account.php
 *
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="my-posts">
		<div class="myposts-title">
			<h2>My saved Properties</h2>
		</div>
	</div>
	<div class="my-property">
		<?php my_saved_property(); ?>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="update-user-wrapper clearfix">
				<div class="myposts-title">
					<h2>Update User</h2>
					<?php echo do_shortcode('[gravityform id="8" title="false" description="false" ajax="true"]'); ?>
				</div>
			<!-- <a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Update User' ) ) ); ?>">Edit My Profile</a> -->
			</div>
		</div>
	</div>
</div>
<!-- #post-## -->
