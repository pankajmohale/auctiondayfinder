<?php
/**
 * Template part for displaying user page content in template-my-account.php
 *
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="my-posts">
		<div class="myposts-title">
			<h2>My Properties</h2>
		</div>
	</div>
	<div class="my-property">
		<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'List your property' ) ) ); ?>">Add Property</a>
		<?php uploaded_property(); ?>
	</div>
	<div class="update-user-wrapper clearfix">
		<div class="myposts-title">
			<h2>Update User</h2>
		</div>
		<?php echo do_shortcode('[gravityform id="8" title="false" description="false" ajax="true"]'); ?>
		<?php 
			$current_user = wp_get_current_user();
			$agent_registration_date = $current_user->user_registered; 
			$next_renewal_date = date('Y-m-d', strtotime($agent_registration_date. ' + 27 day'));
			$next_renewal_date1 = date('Y-m-d', strtotime($agent_registration_date. ' + 28 day'));
			$next_renewal_date2 = date('Y-m-d', strtotime($agent_registration_date. ' + 29 day'));
			//echo $next_renewal_date;
			date_default_timezone_set('Australia/Melbourne');
			$current_system_date = date('Y-m-d', time());
			//echo $current_system_date;
			if($current_system_date == $next_renewal_date){
				$link_address = 'http://localhost/auctiondayfinder/sign-up/';
				echo '<a href="'.$link_address.'">Renew your subscription...</a>';
			}
			elseif($current_system_date == $next_renewal_date1){
				$link_address = 'http://localhost/auctiondayfinder/sign-up/';
				echo '<a href="'.$link_address.'">Renew your subscription...</a>';
			}
			elseif($current_system_date == $next_renewal_date2){
				$link_address = 'http://localhost/auctiondayfinder/sign-up/';
				echo '<a href="'.$link_address.'">Renew your subscription...</a>';
			}
		?><br/>
		<a class="multilisting-update user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Update User' ) ) ); ?>">Edit My Profile</a>
	</div>
</div>
<!-- #post-## -->