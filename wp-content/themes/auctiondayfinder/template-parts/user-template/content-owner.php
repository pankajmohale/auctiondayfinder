<?php
/**
 * Template part for displaying user page content in template-my-account.php
 *
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="my-posts">
		<div class="myposts-title">
			<h2>My Properties</h2>
		</div>
	</div>
	<div class="my-property">
	<?php 
		// $current_user = wp_get_current_user();
		// $user_id = $current_user->ID;					    
		// $user_add_property_val = get_user_meta( $user_id,  'can_add_property', true );
	?>
		<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'List your property' ) ) ); ?>">Add Property</a>
	<!-- <?php if ( $user_add_property_val[0] == 'yes' ) : ?>
		<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Add Single Property' ) ) ); ?>">Add Property</a>
	<?php else : ?>
		<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Add Property' ) ) ); ?>">Add Property</a>
	<?php endif; ?> -->
		<?php uploaded_property(); ?>
	</div>
	<div class="update-user-wrapper clearfix">
	<div class="myposts-title">
		<h2>Update User</h2>
	</div>
	<?php echo do_shortcode('[gravityform id="8" title="false" description="false" ajax="true"]'); ?>
	</div>
</div><!-- #post-## -->
