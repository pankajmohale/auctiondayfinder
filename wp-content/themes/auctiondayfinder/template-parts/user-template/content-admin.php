<?php
/**
 * Template part for displaying user page content in template-my-account.php
 *
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="update-user-wrapper clearfix">
	<div class="myposts-title">
		<h2>Edit Profile</h2>
	</div>
	<?php echo do_shortcode('[gravityform id="8" title="false" description="false" ajax="true"]'); ?>
	</div>
</div><!-- #post-## -->
