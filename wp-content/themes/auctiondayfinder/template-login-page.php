<?php
 /*
 Template Name: Login User Template
 This teplate is only use for login user.
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="col-xs-12">
				<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
			</div>
			<div class="col-sm-12 col-md-12 col-xs-12">
				<?php
					if ( is_user_logged_in() ) {
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'page' );
						endwhile; // End of the loop.					    
					} else { ?>
 					<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Sign in' ) ) ); ?>">Sign in</a>    
				<?php	
					}
				?>
			</div>
		</div><!-- .row -->
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
