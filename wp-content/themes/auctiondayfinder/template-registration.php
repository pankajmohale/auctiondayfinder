<?php
 /*
 Template Name: Registration
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php if ( is_user_logged_in() ) { ?>
			<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'My Account' ) ) ); ?>">My Profile</a>
		<?php } else { ?>
			<div class="row">
				<div class="col-xs-12">
					<div class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div><!-- .entry-header -->
				</div>
				<div class="col-xs-12 sign-up">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
				</div>
				<div class="col-xs-11">
					<div class="custom-sign-up-tabs">
						<ul class="nav nav-tabs nav-tabs-border">
						 	<!-- <li>
						 		<a href="#agent" data-toggle="tab" class="signup-tab tab-sliver border-left-radius">Agent Signup</a>
						 		<span class="arrow-down"></span>
						 	</li> -->
						 	<li class="active">
						 		<a href="#buyer" data-toggle="tab" class="signup-tab tab-sliver border-left-radius">Buyer Signup</a>
						 		<span class="arrow-down"></span>
						 	</li>
						 	<li>
						 		<a href="#seller" data-toggle="tab" class="signup-tab tab-sliver border-right-radius">Seller Signup</a>
						 		<span class="arrow-down"></span>
						 	</li>
						</ul>

						<div class="tab-content">
							<!-- <div id="agent" class="tab-pane fade signup ">
								<?php //echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]'); ?>
							</div>-->
							<div id="buyer" class="tab-pane fade in active signup ">
								<?php echo do_shortcode('[gravityform id="14" title="false" description="false" ajax="true"]'); ?>
							</div>
							<div id="seller" class="tab-pane fade signup ">
								<?php echo do_shortcode('[gravityform id="13" title="false" description="false" ajax="true"]'); ?>

							</div>
							
						</div>
					</div>

				</div>
			</div><!-- .row -->		    
		<?php }?>				
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
