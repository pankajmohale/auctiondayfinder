<?php
 /*
 Template Name: Registration
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php if ( is_user_logged_in() ) { ?>
			<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'My Account' ) ) ); ?>">My Profile</a>
		<?php } else { ?>
			<div class="row">
				<div class="col-xs-12">
					<div class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div><!-- .entry-header -->
				</div>
				<div class="col-sm-8 col-md-8 col-xs-12">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				</div>
				<div class="col-sm-4 col-md-4 col-xs-12" >
					<?php get_sidebar(); ?>	
				</div><!-- .col-md-4>-->
			</div><!-- .row -->		    

		    
		<?php }?>				
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
