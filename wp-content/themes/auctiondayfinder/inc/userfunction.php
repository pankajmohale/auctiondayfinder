<?php 
/* 
* Add custom function
*/
function uploaded_property(){ ?>
		<?php 
			$args = array(
				'post_type' => 'property',
				'author' => get_current_user_id(),
			);
		// the query
		$the_query = new WP_Query( $args ); ?>

	<div class="uploaded-property-wrapper">
		<?php if ( $the_query->have_posts() ) : ?>

		<div class="uploaded-property-list">
			<!-- pagination here -->
			<div class="owl-carousel user_carousel owl-theme">
			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<div class="item">
					<div class="user_property_image">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="user_peoperty_desc">
						<p class="user_property_address">
							<span class="property_plot"> <?php the_field('property_name'); ?></span>
							<span class="property_plot"> <?php the_field('plot_no'); ?></span>
							<span class="property_street"> <?php the_field('street_name'); ?></span>
							<span class="property_suburb"> <?php the_field('suburb'); ?>, </span>
							<span class="property_state"> <?php the_field('state'); ?></span>
							<span class="property_postcode"> <?php the_field('postcode'); ?></span>
						</p>
						<p>
							<span class="property_size"> <?php the_field('land_size_in_sum'); ?></span>
							<span class="property_unit"> <?php the_field('land_size_unit'); ?></span>
							<span class="property_bed"> <?php the_field('bedrooms'); ?></span>
							<span class="property_bath"> <?php the_field('bathrooms'); ?></span>
							<span class="property_car"> <?php the_field('carapaces_or_garage'); ?></span>
						</p>
						<a class="user_property_read_more btn btn-skyblue" href="<?php the_permalink(); ?>">View details</a>
						<a class="user_property_read_more btn btn-skyblue" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Update Post' ) ) ).'?post_id='.get_the_ID(); ?>">Edit</a>
						<a class="user_property_read_more btn btn-skyblue delete_property" property_id="<?php echo get_the_ID(); ?>" current_user_id="<?php echo get_current_user_id(); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					</div>
				</div>
			<?php endwhile; ?>
			<!-- end of the loop -->
			</div>

			<!-- pagination here -->
		</div>

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no property added.' ); ?></p>
		<?php endif; ?>
	</div>

<?php 
}


// This function is used to display saved property of that user.
function my_saved_property(){ ?>
		<?php 
		$save_this_property = get_user_meta( get_current_user_id(), 'save_this_property' , true );	
		$args = array(
			'post_type' => 'property',
		); ?>
		<?php if ($save_this_property) { ?>
	<div class="uploaded-property-list">
		<?php 
			// the query
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- pagination here -->
				<div class="owl-carousel user_carousel owl-theme">
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php if (in_array(get_the_ID(), $save_this_property)) { ?>
							<div class="item">
								<div class="user_property_image">
									<?php the_post_thumbnail(); ?>
								</div>
								<div class="user_peoperty_desc">
									<p class="user_property_address">
										<span class="property_plot"> <?php the_field('property_name'); ?></span>
										<span class="property_plot"> <?php the_field('plot_no'); ?></span>
										<span class="property_street"> <?php the_field('street_name'); ?></span>
										<span class="property_suburb"> <?php the_field('suburb'); ?>, </span>
										<span class="property_state"> <?php the_field('state'); ?></span>
										<span class="property_postcode"> <?php the_field('postcode'); ?></span>
									</p>
									<p>
										<span class="property_size"> <?php the_field('land_size_in_sum'); ?></span>
										<span class="property_unit"> <?php the_field('land_size_unit'); ?></span>
										<span class="property_bed"> <?php the_field('bedrooms'); ?></span>
										<span class="property_bath"> <?php the_field('bathrooms'); ?></span>
										<span class="property_car"> <?php the_field('carapaces_or_garage'); ?></span>
									</p>
									<a class="user_property_read_more btn btn-skyblue" href="<?php the_permalink(); ?>">View details</a>
									<!-- <a class="user_property_read_more btn btn-skyblue" href="						<?php echo esc_url( get_permalink( get_page_by_title( 'Update Post' ) ) ).'?post_id='.get_the_ID(); ?>">Edit Property</a> -->
									<a class="user_property_read_more btn btn-skyblue delete_saved_property" property_id="<?php echo get_the_ID(); ?>" current_user_id="<?php echo get_current_user_id(); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</div>
							</div>
						<?php }	?>
				<?php endwhile; ?>
				<!-- end of the loop -->
				</div>

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
	</div>
		<?php } else {
			echo "No property saved";
		} ?>

<?php 
}