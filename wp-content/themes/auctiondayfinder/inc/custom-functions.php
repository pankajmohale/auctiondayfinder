<?php 
// Add Theme custom function Here 

// Include custom script and css 
add_action( 'wp_enqueue_scripts', 'crunchify_enqueue_script' );
function crunchify_enqueue_script() {
    // Enqueue Custom css 
    wp_register_style( 'bootstrap_min', get_template_directory_uri() .'/css/bootstrap.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_min' ); 

	wp_register_style( 'bootstrap_min_theme', get_template_directory_uri() .'/css/bootstrap-theme.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_min_theme' ); 

    wp_register_style( 'second-style', get_template_directory_uri() .'/css/second-style.css', array(), '21');
    wp_enqueue_style( 'second-style' );


    // Enqueue Custom Js 
    wp_register_script( 'custom_js', get_template_directory_uri() . '/js/custom_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'custom_js' );

	wp_register_script( 'bootstrap_min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
	wp_enqueue_script( 'bootstrap_min' );
}

// New Menu registered
add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'footer_menu', __( 'footer Menu', 'auctionfinder' ) );
}

// Add last and first class to men 
function wpb_first_and_last_menu_class($items) {
    $items[1]->classes[] = 'first';
    $items[count($items)]->classes[] = 'last';
    return $items;
}
add_filter('wp_nav_menu_objects', 'wpb_first_and_last_menu_class');