<?php 
/* extra functiality of gravity form */
// This is Add Property form I have update gallery form value and add inspection date.  
add_action( 'gform_after_submission_10', 'set_post_content', 10, 2 );
add_action( 'gform_after_submission_4', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {
	$post_id = $entry['post_id'];

	$explode_val = explode(']',$entry[59]);
	$explode_val = explode('[',$explode_val[0]);
	$gallery_val = explode(',',$explode_val[1]);

	if ($gallery_val) {
		foreach ($gallery_val as $gallery_key => $gallery_value) {
			// $filename should be the path to a file in the upload directory.
			$filename = trim($gallery_value,'"');

			// The ID of the post this attachment is for.
			$parent_post_id = $post_id;

			// Check the type of file. We'll use this as the 'post_mime_type'.
			$filetype = wp_check_filetype( basename( $filename ), null );

			// Get the path to the upload directory.
			$wp_upload_dir = wp_upload_dir();

			// Prepare an array of post data for the attachment.
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			//set_post_thumbnail( $parent_post_id, $attach_id );

			$media_post[] = $attach_id;
		}
	}

	// Upload media gallery
	if ($media_post) {
		update_field( 'gallery', $media_post , $post_id );
	}


    // Update inspection meta box
    //$post = get_post( $entry['post_id'] );
	/*$inspection_data = unserialize($entry[57]);
	if (is_array($inspection_data)) {
		foreach ($inspection_data as $key => $value) {
			$auction_date = $value[54][1];
			$auction_start_time = $value[55][1];
			$auction_end_time = $value[56][1];
			update_post_meta($post_id, 'inspection_date_'.$key, $auction_date);
			update_post_meta($post_id, 'inspection_start_time_'.$key, $auction_start_time);
			update_post_meta($post_id, 'inspection_end_time_'.$key, $auction_end_time);
		}
	}*/


    //updating post
    // wp_update_post( $post );
	
	// echo $post_id;
	// exit();
	// die();
}

// ================================== Update property post form ====================================
// This function is use for update post form
// I have updated gallery value from this function.
add_action( 'gform_after_submission_6', 'update_post_content', 10, 2 );
function update_post_content( $entry, $form ) {
	$post_id = $entry['post_id'];

	$explode_val = explode(']',$entry[95]);
	$explode_val = explode('[',$explode_val[0]);
	$udate_gallery_val = explode(',',$explode_val[1]);

	if ($udate_gallery_val) {
		foreach ($udate_gallery_val as $udate_gallery_key => $udate_gallery_value) {
			// $filename should be the path to a file in the upload directory.
			$filename = trim($udate_gallery_value,'"');

			// The ID of the post this attachment is for.
			$parent_post_id = $post_id;

			// Check the type of file. We'll use this as the 'post_mime_type'.
			$filetype = wp_check_filetype( basename( $filename ), null );

			// Get the path to the upload directory.
			$wp_upload_dir = wp_upload_dir();

			// Prepare an array of post data for the attachment.
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			//set_post_thumbnail( $parent_post_id, $attach_id );

			$media_post[] = $attach_id;
		}
	}



	$image_ = get_field('gallery', $post_id, false);
	if ($image_ && $media_post) {
		$result = array_merge($image_, $media_post);
		update_field( 'gallery', $result , $post_id );	
	} else {
		update_field( 'gallery', $media_post , $post_id );	
	}
	
	// Upload media gallery
	// if ($media_post) {
	// }


    // Update inspection meta box
    //$post = get_post( $entry['post_id'] );


    //updating post
    // wp_update_post( $post );
}


// add_action( 'gform_after_submission_3', 'regisitration_update_user_meta', 10, 2 );
// function regisitration_update_user_meta( $entry, $form ) {
// 	if ($entry[8] == 'Per Property Subscription' || $entry[4] == 'Individual Property Owner') {
// 		update_user_meta( $user_id, 'some_meta_key', $new_value );
// 	}
// 	echo "<pre>";
// 	print_r($entry);
// 	echo "</pre>";
// }

add_action( 'gform_user_registered', 'regisitration_update_user_meta', 10, 4 );
function regisitration_update_user_meta( $user_id, $feed, $entry, $user_pass ) {
	if ($entry[8] == 'Per Property Subscription' || $entry[4] == 'Individual Property Owner') {
		update_user_meta( $user_id, 'can_add_property', 'yes' );
	}
}

// This is for updating user meta for check is user already added any property. 
add_action( 'gform_after_submission_4', 'update_can_add_proerty_user_meta', 10, 2 );
add_action( 'gform_after_submission_10', 'update_can_add_proerty_user_meta', 10, 2 );
function update_can_add_proerty_user_meta( $entry, $form ) {
	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	update_user_meta( $user_id, 'can_add_property', 'no' );
}

//Update agent subscription type for monthly subscriber
add_action( 'gform_user_registered', 'regisitration_update_subscription_type', 10, 4 );
function regisitration_update_subscription_type( $user_id, $feed, $entry, $user_pass ) {
	if ($entry[8] == 'Monthly Subscription') {
		update_user_meta( $user_id, 'agent_subscription_type', 'Monthly Subscription' );
	}
}

// This function is used for Sending mail to user( Buyer ) who is instered in added property. 
add_action( 'gform_after_submission_4', 'update_user_after_matching_suburb', 10, 2 );
add_action( 'gform_after_submission_10', 'update_user_after_matching_suburb', 10, 2 );
function update_user_after_matching_suburb( $entry, $form ) {
	$args = array(
		'role'	 =>	'subscriber',
	);

	// The Query
	$user_query = new WP_User_Query( $args );

	// User Loop
	if ( ! empty( $user_query->results ) ) {
		foreach ( $user_query->results as $user ) {
			$user_id = $user->ID;					    
			$user_add_property_val = get_user_meta( $user_id,  'suburbs_of_interest', true );
			if ($user_add_property_val == $entry[9]) {
				$to = $user->user_email;
				$subject = 'AuctionDayFinder';
				$body .= "New property added to your area of interest. Please visit property here.";
				$body .= '<br/>Property Name: '.$entry[6].'<br> Please visit property <a href='. get_permalink( $entry['post_id'] ) .'>here</a>.';
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );					
			}
		}
	}
}



add_action( 'gform_after_submission_10', 'update_content_post', 10, 2 );
add_action( 'gform_after_submission_6', 'update_content_post', 10, 2 );
add_action( 'gform_after_submission_4', 'update_content_post', 10, 2 );
function update_content_post( $entry, $form ) {
	$post_id = $entry['post_id'];
	$auction_date_value = date("Ymd", strtotime($entry[26]));
	update_post_meta($post_id, 'auction_date', $auction_date_value);
}