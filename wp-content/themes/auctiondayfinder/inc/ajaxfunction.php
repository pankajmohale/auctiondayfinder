<?php
/* This file is use for adding ajax function */

function array_find($needle, array $haystack)
{
    foreach ($haystack as $key => $value) {
        if (false !== stripos($value, $needle)) {
            $keyva[] = $key;
        }
    }
    return $keyva;
    // return false;
}




/* Get suburb list function */
add_action( 'wp_ajax_nopriv_suburb_loc_function', 'suburb_loc_function' );
add_action( 'wp_ajax_suburb_loc_function', 'suburb_loc_function' );
function suburb_loc_function(){
	$suburb_loc = $_POST['suburb_loc'];
	$property_array = '';
	$property_args = array(
        'post_type' => 'property',
    );

    // the query
    $property_query = new WP_Query( $property_args ); ?>

    <?php if ( $property_query->have_posts() ) : ?>
        <?php while ( $property_query->have_posts() ) : $property_query->the_post(); ?>
            <?php 
            	$property_array[get_post_meta( get_the_ID(), 'suburb', true )] = get_post_meta( get_the_ID(), 'suburb', true );
            ?>
        <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

    <?php else : ?>
        <p><?php _e( 'Sorry, no suburb matched your criteria.' ); ?></p>
    <?php endif;
    $property_unique = array_unique($property_array);
    $searchval = array_find($suburb_loc, $property_unique);?>
	<div class="list-group">
    <?php foreach ($searchval as $key => $value) { ?>
		<a class="list-group-item list-group-item-action suburb-<?php echo $key; ?>" data-value="<?php echo $value; ?>"><?php echo $value; ?></a>
    <?php } ?>
	</div>
<?php 
	die();
}


/* Save property function */
add_action( 'wp_ajax_nopriv_save_property_function', 'save_property_function' );
add_action( 'wp_ajax_save_property_function', 'save_property_function' );
function save_property_function(){
		$property_array = array();
		$property_id = (int)$_POST['proparty_id'];
		$current_user_id = $_POST['current_user_id'];
		
		$save_this_property_arr = get_user_meta( $current_user_id, 'save_this_property', true );

		if(in_array($property_id,$save_this_property_arr)){
			$massage = 'in_array';
		}else{
			if (isset($property_id) && $property_id != '' ) {
				$save_this_property_arr[] = $property_id;
				update_usermeta( $current_user_id, 'save_this_property', $save_this_property_arr );			
			}
			$massage = 'Updated';
		}
		echo $massage;
		die();
}

/* Delete Property function */
add_action( 'wp_ajax_nopriv_delete_property_function', 'delete_property_function' );
add_action( 'wp_ajax_delete_property_function', 'delete_property_function' );
function delete_property_function(){
		$property_id = (int)$_POST['proparty_id'];
		$current_user_id = $_POST['current_user_id'];

		$args = array(
			'post_type' => 'property',
			'author' => $current_user_id,
		);
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>

			<!-- pagination here -->
				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php 
					if ($property_id == get_the_ID() ) {
						wp_delete_post( $property_id, true);
					}
				?>
				<?php endwhile; ?>
				<!-- end of the loop -->
		
			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif;
		
		echo '';
		die();
}


/* Delete Saved Property function */
add_action( 'wp_ajax_nopriv_delete_saved_property_function', 'delete_saved_property_function' );
add_action( 'wp_ajax_delete_saved_property_function', 'delete_saved_property_function' );
function delete_saved_property_function(){
		$property_id = (int)$_POST['proparty_id'];
		$current_user_id = $_POST['current_user_id'];

		$save_this_property = get_user_meta( get_current_user_id(), 'save_this_property' , true );

		foreach ($save_this_property as $key => $value) {
			if ($property_id == $value ) {
				unset($save_this_property[$key]);
			}			
		}
		update_usermeta( $current_user_id, 'save_this_property', $save_this_property);
		
		echo '';
		die();
}
