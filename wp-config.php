<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'auctiondayfinder');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wF$hs`z6h{[[<*FG/#~L#ju`i2>0D{.Zw 1FMowc!=$M(i#=CBA`R yP:h_l7K4o');
define('SECURE_AUTH_KEY',  'bXQ7e/k%*cA+XiGZQeI/u)oiAFgNoN{Xq<66/@Vh4Ews;d_/Z4tTh/`lrAH>/z0H');
define('LOGGED_IN_KEY',    '`?}[ZNNDrUat6~_(d]e9rx_j8P0JRzc}a^F%)o(1ty%zgPyjgvwRhAm6faYp|F/%');
define('NONCE_KEY',        'D!/S7ex1P%nPzRFi>$!(oq1&:rE[{%*1[dn?u{Ku{o86})JaY.%H2RCil5SN+@%Y');
define('AUTH_SALT',        'GSAuJsE&0|~t*O1C1mI-MOWKO~I#100vxF+Sw(]>AfV0-)9|k_sd-N?9HphirRe]');
define('SECURE_AUTH_SALT', 'HR.|G[OlifZJ  g}?*]#P/P@Z8dY&kS?0tA3XX.9OKGhaDIjJH}c`lKEot!b;`:x');
define('LOGGED_IN_SALT',   'P2Y{H=&IKo4qQF9sd:m0hA2Nf+Pz!q1P;3)P?1fB_fk@@)]Pi+#q+puYmkcK~Ba}');
define('NONCE_SALT',       'x?mh)/]j`LlnG]-ZmYz74IG/,y|@(=m<#b!HG{-Q3#Ps6 gN?IQgaG0-50$pO30F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
